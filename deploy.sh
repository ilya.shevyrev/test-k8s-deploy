terraform init
EXTIP=$(terraform apply -var-file=env/test.tfvars -auto-approve | grep "lb_ip" | awk '{print $3}')
echo "Cheking availability of test service on external IP $EXTIP"
curl -I $EXTIP

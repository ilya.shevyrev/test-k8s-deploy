locals {
  cluster_type = "deploy-service"
}

provider "google" {
  version     = "~> 3.16.0"
  region      = var.region
  credentials = file(var.credentials_path)
}

provider "kubernetes" {
  load_config_file       = false
  host                   = module.gke.endpoint
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

data "google_client_config" "default" {
}

module "gcp-network" {
  source       = "terraform-google-modules/network/google"
  version      = "~> 2.3"
  project_id   = var.project_id
  network_name = var.network

  subnets = [
    {
      subnet_name   = var.subnetwork
      subnet_ip     = "10.0.0.0/17"
      subnet_region = var.region
    },
  ]

  secondary_ranges = {
    "${var.subnetwork}" = [
      {
        range_name    = var.ip_range_pods
        ip_cidr_range = "192.168.0.0/18"
      },
      {
        range_name    = var.ip_range_services
        ip_cidr_range = "192.168.64.0/18"
      },
    ]
  }
}

module "gke" {
  source     = "terraform-google-modules/kubernetes-engine/google"
  project_id = var.project_id
  name       = "${local.cluster_type}-cluster${var.cluster_name_suffix}"
  region     = var.region
  network    = module.gcp-network.network_name
  subnetwork = module.gcp-network.subnets_names[0]

  ip_range_pods          = var.ip_range_pods
  ip_range_services      = var.ip_range_services
  create_service_account = false
  service_account        = var.compute_engine_service_account

}

resource "kubernetes_config_map" "conf" {
  metadata {
    name = "conf"
  }

  data = {
    "nginx.conf"   = "${file("${path.module}/conf/nginx.conf")}"
    "test_app.php" = "${file("${path.module}/conf/test_app.php")}"
  }
}

resource "kubernetes_pod" "tf_test_pod" {
  metadata {
    name = "tf-test"

    labels = {
      maintained_by = "terraform"
      app           = "tf-test"
    }
  }

  spec {
    volume {
      name = "conf"
      config_map {
        name = kubernetes_config_map.conf.metadata[0].name
      }
    }

    container {
      image = "nginx:1.7.9"
      name  = "nginx"
      volume_mount {
        name       = "conf"
        mount_path = "/etc/nginx/nginx.conf"
        sub_path   = "nginx.conf"
      }
      volume_mount {
        name       = "conf"
        mount_path = "/var/www/html/index.php"
        sub_path   = "test_app.php"
      }
    }

    container {
      image = "php:7.2-fpm"
      name  = "php-fpm"
      volume_mount {
        name       = "conf"
        mount_path = "/var/www/html/index.php"
        sub_path   = "test_app.php"
      }
    }
  }

  depends_on = [module.gke, kubernetes_config_map.conf]
}

resource "kubernetes_service" "tf_test_svc" {
  metadata {
    name = "tf-test"
  }

  spec {
    selector = {
      app = kubernetes_pod.tf_test_pod.metadata[0].labels.app
    }

    port {
      port        = 80
      target_port = 80
    }

    type = "LoadBalancer"
  }

  depends_on = [module.gke]
}

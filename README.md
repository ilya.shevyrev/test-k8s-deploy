# test-k8s-deploy

### Requirements:
- Platform GKE
- terraform >= 0.12
- gcloud (any GCP supported version)

### Usage:
Put GCP credentials to ../account.json
```
./deploy.sh
```
Destroy test resources:
```
terraform destroy -var-file=env/test.tfvars
```
Example of test output:
```
./deploy.sh

Terraform has been successfully initialized!

Cheking availability of test service on external IP 35.240.110.78
HTTP/1.1 200 OK
Server: nginx/1.7.9
Date: Tue, 19 May 2020 07:33:44 GMT
Content-Type: text/html; charset=UTF-8
Connection: keep-alive
X-Powered-By: PHP/7.2.31
```
